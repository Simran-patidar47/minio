
import os
import PyPDF2
from minio import Minio
from pdf2image import convert_from_path
from minio.error import S3Error
from zipfile import ZipFile
from celery import Celery

UPLOAD_FOLDER = '/home/hp/Downloads'
app = Celery(broker='amqp://guest:guest@localhost:5672', backend='redis://localhost:6379')
client = Minio("172.17.0.1:9000", access_key="minioadmin", secret_key="minioadmin", secure=False)


@app.task(name='tasks.loadfile')
def loadfile(file_name):
    if not client.bucket_exists('test'):
        client.make_bucket('test')

    loc = os.path.join(UPLOAD_FOLDER, file_name)
    client.fput_object('test', file_name, loc)

    client.fput_object('test', file_name, loc)
    return "load"


@app.task(name='tasks.downloadfile')
def downloadfile():
    try:
        client.fget_object('test1', '5104396884_APPRAISAL_151118.pdf', '/home/hp/Downloads/miniofile.pdf')
    except S3Error as e:
        print(e)
        return "bucket not exsist"

    return "done"


@app.task(name='tasks.split_pdf')
def split_pdf(file_name):
    if not client.bucket_exists('test'):
        client.make_bucket('test')


    loc = os.path.join(UPLOAD_FOLDER, file_name)
    images = convert_from_path(loc)
    for i in range(len(images)):
        images[i].save('/home/hp/page' + str(i) + '.jpg', 'JPEG')
        with ZipFile('/home/hp/my_python_files.zip', 'a') as zipobject:
            zipobject.write('/home/hp/page' + str(i) + '.jpg')
    client.fput_object('test', file_name.rsplit('.')[0] + '.zip', '/home/hp/my_python_files.zip')
    return "split done"


@app.task(name='tasks.content_pdf')
def content_pdf(file_name):
    fileobject = open(file_name, 'rb')

    readerobject = PyPDF2.PdfFileReader(fileobject)
    numberofpage = readerobject.numPages
    for i in range(0, numberofpage):
        pageobj = readerobject.getPage(i)
        print(pageobj.extractText())

    fileobject.close()
    return "done"


@app.task(name='tasks.merge_pdf')
def merge_pdf(file1, file2):
    pdfs = [UPLOAD_FOLDER + '/' + file1,
            UPLOAD_FOLDER + '/' + file2]
    mergepdf = PyPDF2.PdfFileMerger()

    for pdf in pdfs:
        mergepdf.append(pdf)

    with open('/home/hp/Downloads/merge.pdf', 'wb') as f:
        mergepdf.write(f)
    return "done"


@app.task(name='tasks.generate_pdf')
def generate_pdf(file_name,numberofpage):
    readerobject = PyPDF2.PdfFileReader(file_name)
    pdf_writer = PyPDF2.PdfFileWriter()
    for i in range(0, numberofpage):
        pageobj = readerobject.getPage(i)
        pdf_writer.addPage(pageobj)
    with open('file_gene.pdf', "wb") as output_file:
        pdf_writer.write(output_file)
    return "done"
